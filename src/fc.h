/*  FreeContact - program to predict protein residue contacts from a sufficiently large multiple alignment
*   Copyright (C) 2012-2013  Laszlo Kajan <lkajan@rostlab.org> Rost Lab, Technical University of Munich, Germany
*   
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*   
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*   
*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FREECONTACT_H
#define FREECONTACT_H

#include <stdint.h>
#include <string>
#include <vector>

class resn_mapper_t {
  public: // immutable
    const uint16_t qs;               // query start
    const std::string q;             // query (all letters from start, lower case letters mean insertion)
    const std::vector<uint16_t> ins; // ins[n] = insertions in insertion-less-q before position n (n is 0-based)
                    resn_mapper_t(uint16_t __qs = 1, const std::string &__q = "") : qs(__qs), q(__q), ins(__q.length(), 0)
    {
        std::vector<uint16_t> &_ins = const_cast<std::vector<uint16_t>&>(ins);
        for(uint16_t i = 0, j = 0, e = q.length(); i < e; ++i)
        {
            if(std::islower(q[i])) ++_ins[j];
            else if(++j < e) _ins[j] = _ins[j-1];
        }
    }
    inline resn_mapper_t& operator=(const resn_mapper_t& __that)
    {
        const_cast<uint16_t&>(qs) = __that.qs;
        const_cast<std::string&>(q) = __that.q;
        const_cast<std::vector<uint16_t>&>(ins) = __that.ins;
        return *this;
    }
    inline uint16_t operator()(uint16_t __i) const
    {
        if(__i < ins.size()) return __i + qs + ins[__i];
        else return __i + qs;
    }
};

#endif // FREECONTACT_H
// vim:et:ts=4:ai:
