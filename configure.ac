#   FreeContact - program to predict protein residue contacts from a sufficiently large multiple alignment
#   Copyright (C) 2012-2013  Laszlo Kajan <lkajan@rostlab.org> Rost Lab, Technical University of Munich, Germany
#   
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#   
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
AC_INIT([freecontact], [1.0.21], [lkajan@rostlab.org])

AC_CONFIG_MACRO_DIR([m4])
AC_CONFIG_SRCDIR([src/freecontact.cpp])
AC_CONFIG_HEADERS([lib/config.h:lib/config.hin])
AM_INIT_AUTOMAKE([no-dist-gzip dist-xz])

AC_PROG_CXX
# AX_LAPACK() needs this
AC_PROG_F77
AC_PROG_FC
AC_FC_LIBRARY_LDFLAGS
AC_PROG_LIBTOOL
AC_LANG([C++])

[{ t2=$(tempfile); t3=$(tempfile);
	trap "rm -f -- '$t2' '$t3'" EXIT
	$CXX -c -Q     --help=optimizers > $t2
	$CXX -c -Q -O3 --help=optimizers > $t3
	# We are especially interested in -ftree-vectorize.
	O3FLAGS=$(diff -u0 $t2 $t3 |grep enabled|sed -e 's/.* \(-f[[:alnum:]-]\+\).*/\1/;'|sed -e ':a; N; s/\n/ /; ta';);
	rm -f -- '$t2' '$t3';
	trap - EXIT;
}]
AC_SUBST(O3FLAGS)
AC_MSG_NOTICE([-O3 enables: $O3FLAGS])

AC_PROG_INSTALL
AX_BOOST_BASE
AX_BOOST_PROGRAM_OPTIONS
AC_CHECK_DECL([HAVE_BOOST_PROGRAM_OPTIONS],[],[AC_MSG_ERROR(could not find Boost::Program_Options)])

AX_OPENMP
AC_SUBST(OPENMP_CFLAGS)
AC_SUBST(OPENMP_CXXFLAGS)
AC_SUBST(OPENMP_FFLAGS)
AC_SUBST(OPENMP_FCFLAGS)

# the following calls AX_BLAS automatically
AX_LAPACK([],[AC_MSG_ERROR(could not find library implementing LAPACK)])

# for XML writing support, requires AX_PTHREAD
AX_PTHREAD([],[AC_MSG_WARN(could not find out how to build POSIX threads)])
AX_LIB_XERCES
AX_XSDCXX
AM_CONDITIONAL([HAVE_XSDCXX], [test -n "$XERCES_LDFLAGS" -a -n "$XSDCXX"])
AM_COND_IF([HAVE_XSDCXX],
	[AC_DEFINE(HAVE_XSDCXX,1,[Define if XSDCXX is enabled])],
    [AC_MSG_NOTICE([XSDCXX is not enabled])
        XERCES_CPPFLAGS=""
        XERCES_LDFLAGS=""
        XERCES_LIBS=""
    ])

AX_EXT

DX_DOT_FEATURE(OFF)
DX_PDF_FEATURE(OFF)
DX_PS_FEATURE(OFF)
DX_INIT_DOXYGEN([libfreecontact], [Doxyfile], [../doxygen-doc])

# lkajan: workaround for bug described below
BIOXSD=BioXSD-1.1
AC_SUBST(BIOXSD)

AC_CONFIG_FILES([
	Makefile
	lib/Doxyfile
	lib/Makefile
	src/Makefile
])
AC_OUTPUT

# lkajan: Workaround for apparent depfile commands (autoconf? libtool?) bug: we get freecontact-$(BIOXSD).Po created instead of the actual content of $(BIOXSD).
[{ if [ -e 'src/.deps/freecontact-$(BIOXSD)_xsd.Po' ]; then
	mv -f 'src/.deps/freecontact-$(BIOXSD)_xsd.Po' "src/.deps/freecontact-${BIOXSD}_xsd.Po";
fi; }]
