FreeContact
===========
This program predicts protein contacts from a sufficiently large multiple alignment.

FreeContact [FC] is based on EVfold-mfDCA [EVfold][mfDCA] and PSICOV [PSICOV].

According to [mcDCA], an effective multiple alignment size of 250 may already be enough.  Version 1.10 of PSICOV uses (effective multiple alignment size) >= (seqlen) as threshold.

For optimal performance, use the Automatically Tuned Linear Algebra Software (ATLAS) library /compiled on the machine/ where freecontact is run.

References
----------
[FC] Submitted.  FreeContact: fast and free direct residue-residue contact prediction.  Kaján L, Sustik MA, Marks DS, Hopf TA, Kalaš M, Rost B.

[EVfold] PLoS One. 2011;6(12):e28766. doi: 10.1371/journal.pone.0028766. Epub 2011 Dec 7.  Protein 3D structure computed from evolutionary sequence variation.  Marks DS, Colwell LJ, Sheridan R, Hopf TA, Pagnani A, Zecchina R, Sander C.

[mfDCA] Proc Natl Acad Sci U S A. 2011 Dec 6;108(49):E1293-301. doi: 10.1073/pnas.1111471108. Epub 2011 Nov 21.  Direct-coupling analysis of residue coevolution captures native contacts across many protein families.  Morcos F, Pagnani A, Lunt B, Bertolino A, Marks DS, Sander C, Zecchina R, Onuchic JN, Hwa T, Weigt M.

[PSICOV] Bioinformatics. 2012 Jan 15;28(2):184-90. Epub 2011 Nov 17.  PSICOV: precise structural contact prediction using sparse inverse covariance estimation on large multiple sequence alignments.  Jones DT, Buchan DW, Cozzetto D, Pontil M.

Depends
-------
 * make check requires Perl.
 * a2m2aln requires Perl.

Static build
------------
There are no rules in the make file to automatically make and install the static version, you have to invoke the rule.
 * Do a full regular build first.
 * You will need the path to a static LAPACK library in STATIC_LAPACK_LIBDIR. Default: /usr/lib/atlas-base.
Do:
$ make -C src freecontact.static
$ ldd src/freecontact.static

Timing
------
* Build:
    make 'CXXFLAGS=-O2 -DRETURN_AFTER_PAIRFREQ'
* Run:
    set core governors to 'performance'
